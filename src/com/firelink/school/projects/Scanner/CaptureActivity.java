/*
 * Copyright (C) 2008 ZXing authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.firelink.school.projects.Scanner;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.EnumSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.firelink.school.projects.Scanner.camera.CameraManager;
import com.firelink.school.projects.Scanner.history.HistoryItem;
import com.firelink.school.projects.Scanner.history.HistoryManager;
import com.firelink.school.projects.database.SQLHelper;
import com.firelink.school.projects.database.OnTaskCompleted;
import com.firelink.school.projects.main.R;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import com.google.zxing.ResultMetadataType;
import com.google.zxing.ResultPoint;
import com.google.zxing.client.result.ParsedResult;
import com.google.zxing.client.result.ResultParser;
import android.provider.*;
import android.graphics.*;
import android.text.format.*;
import android.widget.*;
import android.content.*;

/**
 * This activity opens the camera and does the actual scanning on a background
 * thread. It draws a viewfinder to help the user place the barcode correctly,
 * shows feedback as the image processing is happening, and then overlays the
 * results when a scan is successful.
 * 
 * @author dswitkin@google.com (Daniel Switkin)
 * @author Sean Owen
 */
public final class CaptureActivity extends Activity implements
		SurfaceHolder.Callback, OnTaskCompleted {

	private static final String TAG = CaptureActivity.class.getSimpleName();
	private static final String USER = "Justin";// Settings.Secure.ANDROID_ID;

	public static final int HISTORY_REQUEST_CODE = 0x0000bacc;

	private static final Set<ResultMetadataType> DISPLAYABLE_METADATA_TYPES = EnumSet
			.of(ResultMetadataType.ISSUE_NUMBER,
					ResultMetadataType.SUGGESTED_PRICE,
					ResultMetadataType.ERROR_CORRECTION_LEVEL,
					ResultMetadataType.POSSIBLE_COUNTRY);

	private CameraManager cameraManager;
	private CaptureActivityHandler handler;
	private Result savedResultToShow;
	private ViewfinderView viewfinderView;
	private TextView statusView;
	private View resultView;
	private boolean hasSurface;
	private Collection<BarcodeFormat> decodeFormats;
	private String characterSet;
	private HistoryManager historyManager;
	private InactivityTimer inactivityTimer;
	private BeepManager beepManager;
	private AmbientLightManager ambientLightManager;
	private LinearLayout theLayout;
	private String UPC;
	private Context context;
	private OnTaskCompleted onTask;
	private EditText descriptionEditText;
	private EditText locationEditText;

	ViewfinderView getViewfinderView() {
		return viewfinderView;
	}

	public Handler getHandler() {
		return handler;
	}

	CameraManager getCameraManager() {
		return cameraManager;
	}

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);

		Window window = getWindow();
		window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.capture);

		hasSurface = false;
		historyManager = new HistoryManager(this);
		historyManager.trimHistory();
		inactivityTimer = new InactivityTimer(this);
		beepManager = new BeepManager(this);
		ambientLightManager = new AmbientLightManager(this);

		theLayout = (LinearLayout) findViewById(R.id.database_queries);

		PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
	}

	@Override
	protected void onResume() {
		super.onResume();

		// CameraManager must be initialized here, not in onCreate(). This is
		// necessary because we don't
		// want to open the camera driver and measure the screen size if we're
		// going to show the help on
		// first launch. That led to bugs where the scanning rectangle was the
		// wrong size and partially
		// off screen.
		cameraManager = new CameraManager(getApplication());

		viewfinderView = (ViewfinderView) findViewById(R.id.viewfinder_view);
		viewfinderView.setCameraManager(cameraManager);

		resultView = findViewById(R.id.result_view);
		statusView = (TextView) findViewById(R.id.status_view);

		handler = null;

		resetStatusView();

		SurfaceView surfaceView = (SurfaceView) findViewById(R.id.preview_view);
		SurfaceHolder surfaceHolder = surfaceView.getHolder();
		if (hasSurface) {
			// The activity was paused but not stopped, so the surface still
			// exists. Therefore
			// surfaceCreated() won't be called, so init the camera here.
			initCamera(surfaceHolder);
		} else {
			// Install the callback and wait for surfaceCreated() to init the
			// camera.
			surfaceHolder.addCallback(this);
			surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		}

		beepManager.updatePrefs();
		ambientLightManager.start(cameraManager);

		inactivityTimer.onResume();

		decodeFormats = null;
		characterSet = null;
	}

	@Override
	protected void onPause() {
		if (handler != null) {
			handler.quitSynchronously();
			handler = null;
		}
		inactivityTimer.onPause();
		ambientLightManager.stop();
		cameraManager.closeDriver();
		if (!hasSurface) {
			SurfaceView surfaceView = (SurfaceView) findViewById(R.id.preview_view);
			SurfaceHolder surfaceHolder = surfaceView.getHolder();
			surfaceHolder.removeCallback(this);
		}
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		inactivityTimer.shutdown();
		super.onDestroy();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_BACK:
			setResult(RESULT_CANCELED);
			finish();
			return true;
		case KeyEvent.KEYCODE_FOCUS:
		case KeyEvent.KEYCODE_CAMERA:
			// Handle these events so they don't launch the Camera app
			return true;
			// Use volume up/down to turn on light
		case KeyEvent.KEYCODE_VOLUME_DOWN:
			cameraManager.setTorch(false);
			return true;
		case KeyEvent.KEYCODE_VOLUME_UP:
			cameraManager.setTorch(true);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (resultCode == RESULT_OK) {
			if (requestCode == HISTORY_REQUEST_CODE) {
				int itemNumber = intent.getIntExtra(
						Intents.History.ITEM_NUMBER, -1);
				if (itemNumber >= 0) {
					HistoryItem historyItem = historyManager
							.buildHistoryItem(itemNumber);
					decodeOrStoreSavedBitmap(null, historyItem.getResult());
				}
			}
		}
	}

	private void decodeOrStoreSavedBitmap(Bitmap bitmap, Result result) {
		// Bitmap isn't used yet -- will be used soon
		if (handler == null) {
			savedResultToShow = result;
		} else {
			if (result != null) {
				savedResultToShow = result;
			}
			if (savedResultToShow != null) {
				Message message = Message.obtain(handler,
						R.id.decode_succeeded, savedResultToShow);
				handler.sendMessage(message);
			}
			savedResultToShow = null;
		}
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		if (holder == null) {
			Log.e(TAG,
					"*** WARNING *** surfaceCreated() gave us a null surface!");
		}
		if (!hasSurface) {
			hasSurface = true;
			initCamera(holder);
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		hasSurface = false;
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {

	}

	/**
	 * A valid barcode has been found, so give an indication of success and show
	 * the results.
	 * 
	 * @param rawResult
	 *            The contents of the barcode.
	 * @param scaleFactor
	 *            amount by which thumbnail was scaled
	 * @param barcode
	 *            A greyscale bitmap of the camera data which was decoded.
	 */
	public void handleDecode(Result rawResult, Bitmap barcode, float scaleFactor) {
		inactivityTimer.onActivity();

		boolean fromLiveScan = barcode != null;
		if (fromLiveScan) {
			// historyManager.addHistoryItem(rawResult);
			// Then not from history, so beep/vibrate and we have an image to
			// draw on
			beepManager.playBeepSoundAndVibrate();
			drawResultPoints(barcode, scaleFactor, rawResult);
		}

		handleDecodeInternally(rawResult, barcode);
	}

	/**
	 * Superimpose a line for 1D or dots for 2D to highlight the key features of
	 * the barcode.
	 * 
	 * @param barcode
	 *            A bitmap of the captured image.
	 * @param scaleFactor
	 *            amount by which thumbnail was scaled
	 * @param rawResult
	 *            The decoded results which contains the points to draw.
	 */
	private void drawResultPoints(Bitmap barcode, float scaleFactor,
			Result rawResult) {
		ResultPoint[] points = rawResult.getResultPoints();

		if (points != null && points.length > 0) {
			Canvas canvas = new Canvas(barcode);
			Paint paint = new Paint();
			paint.setColor(getResources().getColor(R.color.result_points));

			if (points.length == 2) {
				paint.setStrokeWidth(4.0f);
				drawLine(canvas, paint, points[0], points[1], scaleFactor);
			} else if (points.length == 4
					&& (rawResult.getBarcodeFormat() == BarcodeFormat.UPC_A || rawResult
							.getBarcodeFormat() == BarcodeFormat.EAN_13)) {
				// Hacky special case -- draw two lines, for the barcode and
				// metadata
				drawLine(canvas, paint, points[0], points[1], scaleFactor);
				drawLine(canvas, paint, points[2], points[3], scaleFactor);
			} else {
				paint.setStrokeWidth(10.0f);
				for (ResultPoint point : points) {
					canvas.drawPoint(scaleFactor * point.getX(), scaleFactor
							* point.getY(), paint);
				}
			}
		}
	}

	private static void drawLine(Canvas canvas, Paint paint, ResultPoint a,
			ResultPoint b, float scaleFactor) {
		canvas.drawLine(scaleFactor * a.getX(), scaleFactor * a.getY(),
				scaleFactor * b.getX(), scaleFactor * b.getY(), paint);
	}

	// Put up our own UI for how to handle the decoded contents.
	private void handleDecodeInternally(Result rawResult, Bitmap barcode) {
		statusView.setVisibility(View.GONE);
		viewfinderView.setVisibility(View.GONE);
		resultView.setVisibility(View.VISIBLE);
		ParsedResult resultHandler = ResultParser.parseResult(rawResult);

		ImageView barcodeImageView = (ImageView) findViewById(R.id.barcode_image_view);
		if (barcode == null) {
			barcodeImageView.setImageBitmap(BitmapFactory.decodeResource(
					getResources(), R.drawable.ic_launcher));
		} else {
			barcodeImageView.setImageBitmap(barcode);
		}

		// Shows the barcode format
		// TextView formatTextView = (TextView)
		// findViewById(R.id.format_text_view);
		// formatTextView.setText(rawResult.getBarcodeFormat().toString());

		// Shows the type of barcode
		// TextView typeTextView = (TextView) findViewById(R.id.type_text_view);
		// typeTextView.setText(resultHandler.getType().toString());

		// Time the barcode was scanned
		// DateFormat formatter =
		// DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);
		// String formattedTime = formatter.format(new
		// Date(rawResult.getTimestamp()));
		// TextView timeTextView = (TextView) findViewById(R.id.time_text_view);
		// timeTextView.setText(formattedTime);

		// Metadata
		TextView metaTextView = (TextView) findViewById(R.id.meta_text_view);
		View metaTextViewLabel = findViewById(R.id.meta_text_view_label);
		metaTextView.setVisibility(View.GONE);
		metaTextViewLabel.setVisibility(View.GONE);
		Map<ResultMetadataType, Object> metadata = rawResult
				.getResultMetadata();
		if (metadata != null) {
			StringBuilder metadataText = new StringBuilder(20);
			for (Map.Entry<ResultMetadataType, Object> entry : metadata
					.entrySet()) {
				if (DISPLAYABLE_METADATA_TYPES.contains(entry.getKey())) {
					metadataText.append(entry.getValue()).append('\n');
				}
			}
			if (metadataText.length() > 0) {
				metadataText.setLength(metadataText.length() - 1);
				metaTextView.setText(metadataText);
				metaTextView.setVisibility(View.VISIBLE);
				metaTextViewLabel.setVisibility(View.VISIBLE);
			}
		}

		// Show the contents of the scan
		TextView contentsTextView = (TextView) findViewById(R.id.contents_text_view);
		CharSequence displayContents = resultHandler.getDisplayResult()
				.replace("r", "");
		contentsTextView.setText(displayContents);
		// Crudely scale betweeen 22 and 32 -- bigger font for shorter text
		int scaledSize = Math.max(22, 32 - displayContents.length() / 4);
		contentsTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, scaledSize);

		// Kill the camera threads to make room for AsyncTasks
		unInit();

		// Assign the dispay to global UPC
		UPC = (String) displayContents;

		// Database stuff
		SQLHelper dbHelp = new SQLHelper();
		context = this;
		onTask = this;
		String query = "SELECT description, last_location, last_update, updated_by "
				+ "FROM android.codes "
				+ "WHERE code = '"
				+ displayContents
				+ "'";

		dbHelp.setQuery(query);
		dbHelp.setFields(new String[] { "description", "last_location",
				"last_update", "updated_by" });
		dbHelp.setDBFunction(SQLHelper.SELECT);
		dbHelp.setURL("jdbc:postgresql://bus1.pointpark.edu/jsmith");
		dbHelp.setUserName("jsmith");
		dbHelp.setPassword("android");
		dbHelp.setContext(context);
		dbHelp.setListener(onTask);
		dbHelp.execute();
	}

	private void unInit() {
		// cameraManager.stopPreview();
		cameraManager.closeDriver();
		handler.quitSynchronously();
		inactivityTimer.shutdown();
		ambientLightManager.stop();
	}

	@Override
	public void onTaskCompleted(String value) {

		if (value == null)
			return;
		if (value.equals("Success")) {
			Toast.makeText(this, "Query Successful", Toast.LENGTH_LONG).show();
			return;
		}

		String[] values;

		if (value == "Failure")
			values = new String[] { "description", "Enter location",
					"last_update", "updated_by" };
		else
			values = value.split("!!@@##");

		Log.d("AndroidRuntime", value);

		// Display the description data found for the entry, if any
		descriptionEditText = (EditText) findViewById(R.id.contents_description_edit_text);
		descriptionEditText.setText(values[0]);
		
		// Display the user who last updated this entry, if there is one
		if (values[3].equals("updated_by")) {
		} else {
			LinearLayout theLayout = (LinearLayout) findViewById(
					R.id.updated_by_text_view).getParent();
			theLayout.setVisibility(View.VISIBLE);

			TextView updatedByTextView = (TextView) findViewById(R.id.updated_by_text_view);
			updatedByTextView.setText(values[3]);
		}

		//The time of the last update
		if (values[2].equals("last_update"))
			values[2] = "Never updated";

		TextView lastUpdateTextView = (TextView) findViewById(R.id.last_update_text_view);
		lastUpdateTextView.setText(values[2]);

		//The location the last time this code was updated
		locationEditText = (EditText) findViewById(R.id.location_edit_text);
		locationEditText.setText(values[1]);
		
		//The button to query the database
		Button sqlButton = (Button) findViewById(R.id.sqlquery_button);
		final String buttonText = (value == "Failure") ? "Insert" : "Update";
		sqlButton.setText(buttonText);
		sqlButton.setAlpha(1.0f);

		sqlButton.setOnClickListener(new dbButtonClick(buttonText));
	}

	private void initCamera(SurfaceHolder surfaceHolder) {
		if (surfaceHolder == null) {
			throw new IllegalStateException("No SurfaceHolder provided");
		}
		if (cameraManager.isOpen()) {
			Log.w(TAG,
					"initCamera() while already open -- late SurfaceView callback?");
			return;
		}
		try {
			cameraManager.openDriver(surfaceHolder);
			// Creating the handler starts the preview, which can also throw a
			// RuntimeException.
			if (handler == null) {
				handler = new CaptureActivityHandler(this, decodeFormats,
						characterSet, cameraManager);
			}
			decodeOrStoreSavedBitmap(null, null);
		} catch (IOException ioe) {
			Log.w(TAG, ioe);
			displayFrameworkBugMessageAndExit();
		} catch (RuntimeException e) {
			// Barcode Scanner has seen crashes in the wild of this variety:
			// java.?lang.?RuntimeException: Fail to connect to camera service
			Log.w(TAG, "Unexpected error initializing camera", e);
			displayFrameworkBugMessageAndExit();
		}
	}

	private void displayFrameworkBugMessageAndExit() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getString(R.string.app_name));
		builder.setMessage(getString(R.string.msg_camera_framework_bug));
		builder.setPositiveButton(R.string.button_ok, new FinishListener(this));
		builder.setOnCancelListener(new FinishListener(this));
		builder.show();
	}

	public void restartPreviewAfterDelay(long delayMS) {
		if (handler != null) {
			handler.sendEmptyMessageDelayed(R.id.restart_preview, delayMS);
		}
		resetStatusView();
	}

	private void resetStatusView() {
		resultView.setVisibility(View.GONE);
		statusView.setText(R.string.msg_default_status);
		statusView.setVisibility(View.VISIBLE);
		viewfinderView.setVisibility(View.VISIBLE);
	}

	public void drawViewfinder() {
		viewfinderView.drawViewfinder();
	}
	
	
	
	/*
	 * Class for database button to update or insert into the database
	 */
	private class dbButtonClick implements View.OnClickListener {

		private final String buttonText;

		public dbButtonClick(String buttonText) {
			this.buttonText = buttonText;
		}

		public void onClick(View p1) {
			String query = "";
			int dbFunction = (buttonText == "Failure") ? SQLHelper.INSERT
					: SQLHelper.UPDATE;
			java.util.Date date = new java.util.Date(System.currentTimeMillis());
			java.sql.Timestamp timeStamp = new java.sql.Timestamp(
					date.getTime());

			if (buttonText.equals("Failure")) {
				query = "INSERT INTO ANDROID.CODES " + 
						"VALUES('" + descriptionEditText.getText() + "', " +
							   "'" + locationEditText.getText() + "', " +
							   "'" + timeStamp + "', " +
							   "'" + USER + "');";
			} else {
				query = "UPDATE ANDROID.CODES " + 
						"SET description = '" + descriptionEditText.getText() + "', " +
								"last_location = '" + locationEditText.getText() + "', " +
								"last_update = '" + timeStamp + "', " +
								"updated_by = '" + USER + "' " + 
								"WHERE CODE = '" + UPC + "';";
			}

			SQLHelper dbHelp = new SQLHelper();

			dbHelp.setQuery(query);
			// dbHelp.setFields(new String[]{"description", "last_location",
			// "last_update", "updated_by"});
			dbHelp.setDBFunction(dbFunction);
			dbHelp.setURL("jdbc:postgresql://bus1.pointpark.edu/jsmith");
			dbHelp.setUserName("jsmith");
			dbHelp.setPassword("android");
			dbHelp.setContext(context);
			dbHelp.setListener(onTask);
			dbHelp.execute();
		}
	};

}
