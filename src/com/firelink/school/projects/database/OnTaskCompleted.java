package com.firelink.school.projects.database;

public abstract interface OnTaskCompleted {
	
	void onTaskCompleted(String value);

}
