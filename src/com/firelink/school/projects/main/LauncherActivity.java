package com.firelink.school.projects.main;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;

import com.firelink.school.projects.project1.Project_1;
import com.firelink.school.projects.project2.Project_2;
import com.firelink.school.projects.project3.Project_3;

public class LauncherActivity extends Activity 
{
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        
        ActionBar actionBar = getActionBar();
        
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        
        String label1 = "Project 1 - JDBC";
        Tab tab = actionBar.newTab();
        tab.setText(label1);
        TabListener<Project_1> t1 = new TabListener<Project_1>(this, label1, com.firelink.school.projects.project1.Project_1.class);
        tab.setTabListener(t1);
        actionBar.addTab(tab);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayUseLogoEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);
        
        String label2 = "Project 2 - JSON";
        tab = actionBar.newTab();
        tab.setText(label2);
        TabListener<Project_2> t2 = new TabListener<Project_2>(this, label2, com.firelink.school.projects.project2.Project_2.class);
        tab.setTabListener(t2);
        actionBar.addTab(tab);
        
        String label3 = "Project 3 - Bar/QR CODE";
        tab = actionBar.newTab();
        tab.setText(label3);
        TabListener<Project_3> t3 = new TabListener<Project_3>(this, label3, com.firelink.school.projects.project3.Project_3.class);
        tab.setTabListener(t3);
        actionBar.addTab(tab);
//        
//        tab = actionBar.newTab();
//        tab.setText("tab three");
//        TabListener<Tab3Fragment> t3 = new TabListener<Tab3Fragment>(this, "tab three", Tab3Fragment.class);
//        tab.setTabListener(t3);
//        actionBar.addTab(tab);
//        
//        tab = actionBar.newTab();
//        tab.setText("tab 4");
//        TabListener<Tab4Fragment> t4 = new TabListener<Tab4Fragment>(this, "tab 4", Tab4Fragment.class);
//        tab.setTabListener(t4);
//        actionBar.addTab(tab);
    }
    
    private class TabListener<T extends Fragment> implements ActionBar.TabListener
    {
    	private Fragment mFragment;
    	private final Activity mActivity;
    	private final String mTag;
    	private final Class<T> mClass;
    	
    	TabListener(Activity activity, String tag, Class<T> clz)
    	{
    		mActivity = activity;
    		mTag = tag;
    		mClass = clz;
    	}

		public void onTabReselected(Tab tab, FragmentTransaction ft) 
		{
			//TODO
		}

		public void onTabSelected(Tab tab, FragmentTransaction ft) 
		{
			//Check if fragment already exists
			if(mFragment == null)
			{
				//If not, do this
				mFragment = Fragment.instantiate(mActivity, mClass.getName());
				ft.add(android.R.id.content, mFragment, mTag);
			}
			else
			{
				//If it does, attach it
				ft.attach(mFragment);
			}
		}

		public void onTabUnselected(Tab tab, FragmentTransaction ft) 
		{
			if(mFragment != null)
			{
				//Detach
				ft.detach(mFragment);
			}
		}
    }
}
