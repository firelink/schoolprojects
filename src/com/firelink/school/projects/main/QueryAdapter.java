package com.firelink.school.projects.main;

import com.firelink.school.projects.main.R;

import android.app.*;
import android.content.*;
import android.graphics.*;
import android.util.*;
import android.view.*;
import android.widget.*;

public class QueryAdapter extends ArrayAdapter<String>
{
	private final Context context;
	private final Activity activity;
	private final SparseArray<String> data;
	private int numOfColumns;
	private int dataCount;
	private int arbCount;
	private TextView[] tvs;
	private String delim;

	public QueryAdapter(Context context, Activity activity) 
	{ 
		super(context, R.layout.adapter_view); 
		this.context = context; 
		this.activity = activity;
		data = new SparseArray<String>();
		dataCount = 0;
		arbCount = 0;
	}
	
	public void add(String object)
	{
		super.add(object);
		data.put(dataCount, object);
		dataCount++;
	}
	
	public void add(String[] objects)
	{
		for(int i = 0; i < objects.length; i++)
		{
			super.add(objects[i]);
			data.put(dataCount, objects[i]);
			dataCount++;
		}
	}
	
	public void setColumns(String[] columns)
	{
		for(int i = 0; i < columns.length; i++)
		{
			super.add(columns[i]);
			data.put(dataCount, columns[i]);
			dataCount++;
		}
		//this.columns = columns;
		numOfColumns = columns.length;
	}
	
	public void setDelimiter(String delim)
	{
		this.delim = delim;
	}

	public View getView(int p, View v, ViewGroup vg)
	{
		LayoutInflater vi = 
			(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = vi.inflate(R.layout.adapter_view, vg, false);
		
		//TextView tv1 = (TextView)view.findViewById(R.id.cust_label1);
		LinearLayout holder = (LinearLayout)view.findViewById(R.id.adapter_holder);
		tvs = new TextView[numOfColumns];
		LinearLayout.LayoutParams lpr = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
		lpr.weight = 1.0f / numOfColumns;
		lpr.gravity = Gravity.CENTER;
		//lpr.leftMargin = 400 / numOfColumns;
		
		if(0 == p)
		{
			for(int i = 1; i <= numOfColumns; i++)
			{
				tvs[i-1] = new TextView(activity);
				tvs[i-1].setText(data.get(i-1) + "");
				tvs[i-1].setTextSize(100 / numOfColumns);
				tvs[i-1].setLayoutParams(lpr);
				tvs[i-1].setBackgroundColor(Color.BLACK);
				tvs[i-1].setTextColor(Color.WHITE);
				tvs[i-1].setGravity(Gravity.CENTER);
				holder.addView(tvs[i-1]);
			}
		}
		
		if(p >= numOfColumns)
		{
			String temp[] = data.get(p).split(delim);
			try
			{
				if (temp.length != numOfColumns)
					throw new Exception("Invalid delimter. String size exceeds size of column count");
			}
			catch (Exception e)
			{}
			
			for(int i = 0; i < numOfColumns; i++)
			{
				tvs[i] = new TextView(activity);
				tvs[i].setText(temp[i]);
				tvs[i].setTextSize(100 / numOfColumns);
				tvs[i].setLayoutParams(lpr);
				//tvs[i].setBackgroundColor(Color.WHITE);
				tvs[i].setTextColor(Color.BLACK);
				tvs[i].setGravity(Gravity.CENTER);
				holder.addView(tvs[i]);
			}
		}
		
		if(arbCount >= data.size())
		{
			arbCount = 0;
		}
		
		//arbCount++;
		
		Log.d("AndroidRuntime", ""+p + " : " + data.get(p));
//		TextView tv1 = new TextView(activity);
//		holder.addView(tv1);
//		tv1.setText("" + data.get(p));
		
		//tv.setVisibility(View.GONE);

		return (view);
	}
}
