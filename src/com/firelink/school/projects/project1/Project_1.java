package com.firelink.school.projects.project1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;

import com.firelink.school.projects.main.R;
import com.firelink.school.projects.main.QueryAdapter;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
//import org.sqldroid.SQLDroidDriver;

public class Project_1 extends Fragment
{
	TextView resultArea;
	QueryAdapter adapt;
	LinearLayout theLayout;
	
	public Project_1()
	{
	
	}
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
        super.onCreateView(inflater, container, savedInstanceState);
        container.removeAllViews();
        theLayout = (LinearLayout)inflater.inflate(R.layout.project_1, container, false);

		resultArea = (TextView)theLayout.findViewById(R.id.dataTextView);
		resultArea.setText("Project 1 - Database Querying with JDBC");
		Button btn = (Button)theLayout.findViewById(R.id.dataButton);
		adapt = new QueryAdapter(getActivity(), getActivity());

		btn.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View p1)
			{
				new SQLUpdates().execute();
				//dataConnection();
			}
		});
		
		return theLayout;
    }

	private class SQLUpdates extends AsyncTask<Void,Void,String> 
	{
		@Override
		protected String doInBackground(Void... params) 
		{
			String retval = "Failure";
			try 
			{
				Class.forName("org.postgresql.Driver");
				
				String url = "jdbc:postgresql://bus1.pointpark.edu/jsmith?user=jsmith&password=android";
				
				DriverManager.setLoginTimeout(5);
				Connection conn = DriverManager.getConnection(url);
				Statement state = conn.createStatement();
				//String sql2 = "INSERT into android.users(firstname, lastname, email) VALUES('Jesse', 'Padjune', 'jtpadju@pointpark.edu')";
				//st.executeUpdate(sql2);
				String query = "SELECT * from android.users";
				ResultSet results = state.executeQuery(query);
				ResultSetMetaData meta = results.getMetaData();
				String[] columns = new String[meta.getColumnCount()];

				for(int i = 0; i < columns.length; i++)
				{
					columns[i] = meta.getColumnName(i+1);
				}

				String delimiter = "@!#";
				adapt.setColumns(columns);
				adapt.setDelimiter(delimiter);

				while (results.next()) 
				{
					retval = results.getString(1);
					String temp = "";
					for(int i = 0; i < columns.length; i++)
					{
						if(i < columns.length - 1)
							temp += results.getString(i+1) + delimiter;
						else
							temp += results.getString(i+1);
					}

					adapt.add(temp);
				}

				results.close();
				state.close();
				conn.close();
				
			}
			catch (Exception e) 
			{
				Log.e("AndroidRuntime", "Error", e); 
				e.printStackTrace();
				retval = e.toString();
			}

			return retval;
		}

		@Override
		protected void onPostExecute(String value) 
		{
			if(!value.equals("Failure"))
			{
				ListView lv = (ListView)theLayout.findViewById(R.id.dataListView);
				lv.setAdapter(adapt);
			}
			//resultArea.setText(value);
		}
	}
}
