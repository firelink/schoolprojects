package com.firelink.school.projects.project2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONObject;

import com.firelink.school.projects.main.R;
import com.firelink.school.projects.main.QueryAdapter;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.view.*;
//import org.sqldroid.SQLDroidDriver;

public class Project_2 extends Fragment
{
	private final int POSTGRES = 1;
	private final int MYSQL = 2;
	
	private int dbType;
	private String theURL;
	
	private TextView resultArea;
	private QueryAdapter adapt;
	private LinearLayout theLayout;
	private ListView lv;
	private EditText editHost;
	
	public Project_2()
	{
		theURL = "http://192.168.1.200/test.php";
		dbType = POSTGRES;
	}
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
        super.onCreateView(inflater, container, savedInstanceState);
        container.removeAllViews();
        //ArrayAdapter
        adapt = new QueryAdapter(getActivity(), getActivity());
        //Layout
        theLayout = (LinearLayout)inflater.inflate(R.layout.project_2, container, false);
        //ResultArea TextView
		resultArea = (TextView)theLayout.findViewById(R.id.dataTextView);
		resultArea.setText("Project 2 - Database Querying with JSON");
		//ListView
		lv = (ListView)theLayout.findViewById(R.id.dataListView);
		//Button
		Button btn = (Button)theLayout.findViewById(R.id.dataButton);
		btn.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View p1)
			{
				lv.setAdapter(null);
				adapt = new QueryAdapter(getActivity(), getActivity());
				new SQLUpdates().execute();
			}
		});
		//Spinner
		String[] dbTypeArray = {"PostgresQL",
								"MySQL"		};
		Spinner spin = (Spinner)theLayout.findViewById(R.id.dataSpinDBType);
		spin.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line, dbTypeArray));
		spin.setOnItemSelectedListener(new OnItemSelectedListener() 
		{
			public void onItemSelected(AdapterView<?> adapter, View view,
					int position, long id) 
			{
				switch(position)
				{
					case 0:
						dbType = POSTGRES;
						break;
						
					case 1:
						dbType = MYSQL;
						break;
				}
			}

			public void onNothingSelected(AdapterView<?> arg0) 
			{}
			
		});
		
		//TextView host
		TextView textHost = (TextView)theLayout.findViewById(R.id.dataTextHost);
		
		//EditText host
		editHost = (EditText)theLayout.findViewById(R.id.dataEditHost);
		editHost.setText(theURL);
		
		return theLayout;
    }

	private class SQLUpdates extends AsyncTask<Void,Void,String> 
	{
		@Override
		protected String doInBackground(Void... params) 
		{
			try 
			{
				theURL = editHost.getText().toString();
				HttpParams httpParams = new BasicHttpParams();
				HttpConnectionParams.setConnectionTimeout(httpParams, 1);
				HttpConnectionParams.setSoTimeout(httpParams, 1);
				//
				HttpClient httpClient = new DefaultHttpClient();
				String url = theURL;
				HttpPost httpPost = new HttpPost(url);
				
				try
				{
					List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
					nameValuePairs.add(new BasicNameValuePair("db_type", dbType + ""));
					httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
					ResponseHandler<String> responseHandler = new BasicResponseHandler();
					String responseBody = httpClient.execute(httpPost, responseHandler);
					
					JSONObject json = new JSONObject(responseBody);
					JSONArray jArray = json.getJSONArray("rows");
					
					String delimiter = "@!#";
					adapt.setDelimiter(delimiter);
					
					{
						JSONObject e = jArray.getJSONObject(0);
						String s = e.getString("row");
						JSONObject jObject = new JSONObject(s);
						JSONArray columns1 = jObject.names();
							
						String[] columns2 = new String[columns1.length()];
						
						for(int i = 0; i < columns2.length; i++)
						{
							columns2[i] = columns1.getString(i); 
						}
						
						adapt.setColumns(columns2);
					}
					
					for(int i = 0; i < jArray.length(); i++)
					{
						JSONObject e = jArray.getJSONObject(i);
						String s = e.getString("row");
						JSONObject jObject = new JSONObject(s);
						JSONArray columns = jObject.names();
						String temp = "";
						
						for(int j = 0; j < columns.length(); j++)
						{
							temp += jObject.getString(columns.getString(j)) + delimiter;
						}

						adapt.add(temp);
					}
					
				}catch(ClientProtocolException e)
				{
					e.printStackTrace();
				}catch(IOException e)
				{
					e.printStackTrace();
				}
			}
			catch (Exception e) 
			{
				Log.e("AndroidRuntime", "Error", e); 
				e.printStackTrace();
				//retval = e.toString();
			}

			return "";
		}

		@Override
		protected void onPostExecute(String value) 
		{
			if(!value.equals("Failure"))
			{
				lv.setAdapter(adapt);
			}
			//resultArea.setText(value);
		}
	}
}
