package com.firelink.school.projects.project3;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.firelink.school.projects.Scanner.CaptureActivity;
import com.firelink.school.projects.main.R;

//import org.sqldroid.SQLDroidDriver;

public class Project_3 extends Fragment
{
	
	public static final int UPDATE = 0;
	public static final int INSERT = 1;
	
	
	private TextView headingText;
	private LinearLayout theLayout;
	private static Fragment frag;
	private TextView textResultArea;
	private Button dbButton;
	private EditText dbEditText;
	private TextView dbTextView;
	private TextView textResults;
	private String query;
	private int dbFunction = 1;
	
	public Project_3()
	{
	
	}
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
        super.onCreateView(inflater, container, savedInstanceState);
        container.removeAllViews();
        theLayout = (LinearLayout)inflater.inflate(R.layout.project_3, container, false);

		headingText = (TextView)theLayout.findViewById(R.id.dataTextView);
		headingText.setText("Project 3 - Bar/QR Code Scanner");
		textResultArea = (TextView)theLayout.findViewById(R.id.dataTextViewResult);
		dbEditText = (EditText)theLayout.findViewById(R.id.dataEditText);
		dbEditText.setVisibility(View.GONE);
		dbTextView = (TextView)theLayout.findViewById(R.id.dataTextViewDescription);
		dbTextView.setVisibility(View.GONE);
		textResults = (TextView)theLayout.findViewById(R.id.dataResults);
		textResults.setVisibility(View.GONE);
		
		
		Button btn = (Button)theLayout.findViewById(R.id.dataButton);
		btn.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View p1)
			{
				//IntentIntegratorV30 intent = new IntentIntegratorV30(frag);
				//intent.initiateScan();
				//Intent intent = new Intent(getActivity(), CaptureActivity.class);
				//startActivityForResult(intent, 0);
				Intent intent = new Intent(getActivity(), CaptureActivity.class);
				startActivityForResult(intent, 0);
			}
		});
		
		return theLayout;
    }
	
    public void onActivityResult(int requestCode, int resultCode, Intent intent) 
    {
       if (requestCode == 0) {
          if (resultCode == Activity.RESULT_OK) 
          {
        	 String upc = "Barcode: " + intent.getStringExtra("SCAN_RESULT");
    	     String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
    	    	 
    	     dbEditText.setVisibility(View.VISIBLE);
    	     dbTextView.setVisibility(View.VISIBLE);
    	     textResultArea.setText(upc);
          } 
          else if (resultCode == Activity.RESULT_CANCELED) 
          {
        	  
          }
       }
    }
}

